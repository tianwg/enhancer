package com.exercise.enhancer.clients;

import com.exercise.enhancer.EnhancerApplicationTests;
import com.exercise.enhancer.domain.Demographic;
import com.exercise.enhancer.domain.Publisher;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ServiceClientTest {

  @Autowired private IPClient ipClient;
  @Autowired private DemographicClient demographicClient;
  @Autowired private PublisherClient publisherClient;

  @Test
  @Ignore
  public void testIPClient() {
    // test ip from canada
    Assert.assertTrue(ipClient.isBlocked("23.83.223.255"));
    // test ip from usa
    Assert.assertFalse(ipClient.isBlocked("172.217.6.195"));
  }

  @Test
  @Ignore
  public void testPublisherClient() {
    Publisher expected = new Publisher("ksjdf9325", "ACME Inc.");
    Assert.assertEquals(expected, publisherClient.getPublisher("foo123"));
  }

  @Test
  @Ignore
  public void testDemographicClient() {
    Demographic expected = new Demographic("49", "51");
    Assert.assertEquals(expected, demographicClient.getDemographic("foo123"));
  }
}
