package com.exercise.enhancer;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RunWith(SpringRunner.class)
@SpringBootTest()
@WebAppConfiguration
@ContextConfiguration(classes = EnhancerApplication.class)
@Slf4j
public class EnhancerApplicationTests {

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @Before
  public void setup() throws Exception {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    this.mockMvc.perform(post("/api/enhancer").contentType(MediaType.APPLICATION_JSON).content(requestBody()));
  }


  /**
   * Test 51 concurrent requests
   * */
  @Test
  public void testConcurrentRequests() {
      ExecutorService executor= Executors.newFixedThreadPool(50);
      try{
        for ( int i=0; i < 50; i++){
        executor.execute(
             () -> {
              try {
                mockMvc.perform(
                    post("/api/enhancer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody())).andReturn();
              } catch (Exception e) {
                e.printStackTrace();
              }
            });
        }
      }catch(Exception err){
        err.printStackTrace();
      }
      executor.shutdown();
  }

  /**
   * Input with the request, and test the expected augmented result.
   * */
  @Test
  public void testAugmentedResult() throws Exception {
    mockMvc.perform(
                    post("/api/enhancer").contentType(MediaType.APPLICATION_JSON).content(requestBody()))
            .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.content().json(expectedResponse()));
  }

  /**
   *
   * Not able to automate test for now, Since every siteID seems all have results. But did some hard coded test by
   * changing url to non exist endpoint.
   * */
  @Test
  public void testAbortIfNoPublisher() {

  }

  private String requestBody() throws JsonProcessingException {

    return "{\n"
        + "   \"site\":{\n"
        + "      \"id\":\"foo123\",\n"
        + "      \"page\":\"http://www.foo.com/why-foo\"\n"
        + "   },\n"
        + "   \"device\":{\n"
        + "      \"ip\":\"69.250.196.118\"\n"
        + "   },\n"
        + "   \"user\":{\n"
        + "      \"id\":\"9cb89r\"\n"
        + "   }\n"
        + "}";
  }

  private String expectedResponse() throws IOException {
    return "{\n" +
            "    \"user\": {\n" +
            "        \"id\": \"9cb89r\"\n" +
            "    },\n" +
            "    \"device\": {\n" +
            "        \"ip\": \"69.250.196.118\",\n" +
            "        \"geo\": {\n" +
            "            \"country\": \"US\"\n" +
            "        }\n" +
            "    },\n" +
            "    \"site\": {\n" +
            "        \"id\": \"foo123\",\n" +
            "        \"page\": \"http://www.foo.com/why-foo\",\n" +
            "        \"demographic\": {\n" +
            "            \"femalePercent\": \"49\",\n" +
            "            \"malePercent\": \"51\"\n" +
            "        }\n" +
            "    },\n" +
            "    \"publisher\": {\n" +
            "        \"id\": \"ksjdf9325\",\n" +
            "        \"name\": \"ACME Inc.\"\n" +
            "    }\n" +
            "}";
  }
}
