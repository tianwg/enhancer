package com.exercise.enhancer.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
@EnableScheduling
public class RestTemplateConfig {

  @Autowired
  private ApplicationProperties applicationProperties;

  @Bean
  public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
    Registry<ConnectionSocketFactory> socketFactoryRegistry =
        RegistryBuilder.<ConnectionSocketFactory>create()
            .register("http", new PlainConnectionSocketFactory())
            .build();
    PoolingHttpClientConnectionManager poolingHttpClientConnectionManager =
        new PoolingHttpClientConnectionManager(socketFactoryRegistry);
    poolingHttpClientConnectionManager.setMaxTotal(
        applicationProperties.getHttpClient().getMaxTotalConnection());
    poolingHttpClientConnectionManager.setDefaultMaxPerRoute(
        applicationProperties.getHttpClient().getMaxPerRoute());
    return poolingHttpClientConnectionManager;
  }

  @Bean
  public ConnectionKeepAliveStrategy keepAliveStrategy() {
    return new DefaultConnectionKeepAliveStrategy();
  }

  @Bean
  public CloseableHttpClient httpClient() {
    RequestConfig.Builder builder = RequestConfig.custom();
    builder
        .setConnectionRequestTimeout(applicationProperties.getHttpClient().getRequestTimeout())
        .setConnectTimeout(applicationProperties.getHttpClient().getConnectTimeout())
        .setSocketTimeout(applicationProperties.getHttpClient().getSocketTimeout());
    RequestConfig requestConfig = builder.build();
    return HttpClients.custom()
        .setDefaultRequestConfig(requestConfig)
        .setConnectionManager(poolingHttpClientConnectionManager())
        .build();
  }

  @Bean
  public HttpComponentsClientHttpRequestFactory clientHttpRequestFactory() {
    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory =
        new HttpComponentsClientHttpRequestFactory();
    clientHttpRequestFactory.setHttpClient(httpClient());
    return clientHttpRequestFactory;
  }

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate(clientHttpRequestFactory());
  }

  @Bean
  public Runnable connectionMonitor(final PoolingHttpClientConnectionManager connectionManager) {
    return new Runnable() {
      @Override
      @Scheduled(fixedDelay = 5 * 60 * 1000)
      public void run() {
        try {
          if (connectionManager != null) {
            log.info("Closing idle or expired connections");
            connectionManager.closeExpiredConnections();
            connectionManager.closeIdleConnections(20, TimeUnit.SECONDS);
          }
        } catch (Exception e) {
          log.error("Exception when closing connections " + e.getMessage());
        }
      }
    };
  }

  @Bean
  public TaskScheduler taskScheduler() {
    ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
    scheduler.setPoolSize(applicationProperties.getHttpClient().getSchedulerPool());
    scheduler.setThreadNamePrefix("scheduler-pool-");
    return scheduler;
  }
}
