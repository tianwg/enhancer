package com.exercise.enhancer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
  private PublisherService publisherService = new PublisherService();
  private DemographicService demographicService = new DemographicService();
  private HttpClient httpClient = new HttpClient();
  private AsyncTaskPool asyncTaskPool = new AsyncTaskPool();
  private IPService ipService = new IPService();

  @Data
  public static class PublisherService {
    private String url;
    private String apiKey;
  }

  @Data
  public static class DemographicService {
    private String url;
    private String apiKey;
  }

  @Data
  public static class HttpClient {
    private int maxTotalConnection;
    private int maxPerRoute;
    private int requestTimeout;
    private int connectTimeout;
    private int socketTimeout;
    private int idleTimeout;
    private int schedulerPool;
  }

  @Data
  public class AsyncTaskPool {
    private int coreSize;
    private int maxSize;
    private int queueCapacity;
  }

  @Data
  public class IPService {
    private int account;
    private String licenseKey;
  }
}
