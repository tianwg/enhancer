package com.exercise.enhancer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@Slf4j
public class AsyncConfig {
  @Autowired
  private ApplicationProperties applicationProperties;

  @Bean("asyncExecutor")
  public Executor asyncExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setThreadNamePrefix("Async-task-executor-");
    executor.setCorePoolSize(applicationProperties.getAsyncTaskPool().getCoreSize());
    executor.setMaxPoolSize(applicationProperties.getAsyncTaskPool().getMaxSize());
    executor.setQueueCapacity(applicationProperties.getAsyncTaskPool().getQueueCapacity());
    executor.initialize();
    log.info("Async task executor initialized.");
    return executor;
  }
}
