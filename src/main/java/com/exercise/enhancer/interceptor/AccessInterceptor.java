package com.exercise.enhancer.interceptor;

import com.exercise.enhancer.clients.IPClient;
import com.exercise.enhancer.exception.NotAllowedException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Slf4j
public class AccessInterceptor extends HandlerInterceptorAdapter {

  @Autowired IPClient ipClient;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {

    long startTime = System.currentTimeMillis();
    MDC.put("RequestId", UUID.randomUUID().toString());

    MDC.put("startTime", String.valueOf(startTime));

    String ipAddress = request.getHeader("X-FORWARDED-FOR");
    if (ipAddress == null) {
      ipAddress = request.getRemoteAddr();
    }
    boolean blocked = ipClient.isBlocked(ipAddress);
    boolean isLocal = request.getLocalAddr().equals(ipAddress);
    log.info("Incoming request from IP : {}. Blocked: {}", ipAddress, blocked && !isLocal);
    if (blocked && !isLocal) {
      throw new NotAllowedException();
    }
    return true;
  }

  @Override
  public void afterCompletion(
      HttpServletRequest request,
      HttpServletResponse response,
      Object handler,
      @Nullable Exception ex)
      throws Exception {
    String startTime = MDC.get("startTime");
    long endTime = System.currentTimeMillis();
    long execTime = endTime - Long.parseLong(startTime);
    log.info("Execution time {} ms", String.valueOf(execTime));
  }
}
