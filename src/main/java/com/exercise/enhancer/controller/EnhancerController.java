package com.exercise.enhancer.controller;

import com.exercise.enhancer.domain.AdEntity;
import com.exercise.enhancer.service.EnhancerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class EnhancerController {
  @Autowired EnhancerService enhancerService;

  @RequestMapping(
    value = "/enhancer",
    method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  ResponseEntity<AdEntity> getEnhancedResult(@RequestBody AdEntity adEntity) throws Exception {
    AdEntity result = enhancerService.enhanceEntity(adEntity);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }
}
