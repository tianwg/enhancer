package com.exercise.enhancer.controller;

import com.exercise.enhancer.exception.ErrorMessage;
import com.exercise.enhancer.exception.NotAllowedException;
import com.exercise.enhancer.exception.PublisherNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

  @ExceptionHandler({NotAllowedException.class})
  public ResponseEntity<ErrorMessage> handleNotAllowedException(NotAllowedException e) {
    return new ResponseEntity<>(new ErrorMessage(e.getMessage()), HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler({PublisherNotFoundException.class})
  public ResponseEntity<ErrorMessage> handlePublisherNotFoundException(
      PublisherNotFoundException e) {
    return new ResponseEntity<>(new ErrorMessage(e.getMessage()), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler({Exception.class})
  public ResponseEntity<ErrorMessage> handleGlobalException(Exception e) {
    log.error("Exception " + e);
    return new ResponseEntity<>(new ErrorMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
