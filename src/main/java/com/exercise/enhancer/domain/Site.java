package com.exercise.enhancer.domain;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Site {
  @NotNull private String id;
  @NotNull private String page;
  private Demographic demographic;
}
