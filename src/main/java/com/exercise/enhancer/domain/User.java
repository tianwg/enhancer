package com.exercise.enhancer.domain;

import lombok.Data;

@Data
public class User {
  private String id;
}
