package com.exercise.enhancer.domain;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AdEntity {
  private User user;
  @NotNull private Device device;
  @NotNull private Site site;
  private Publisher publisher;
}
