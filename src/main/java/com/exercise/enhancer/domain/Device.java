package com.exercise.enhancer.domain;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Device {
  @NotNull private String ip;
  private Geo geo;
}
