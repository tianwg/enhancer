package com.exercise.enhancer;

import com.exercise.enhancer.config.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
@EnableConfigurationProperties({ApplicationProperties.class})
public class EnhancerApplication {

  public static void main(String[] args) {
    SpringApplication.run(EnhancerApplication.class, args);
  }
}
