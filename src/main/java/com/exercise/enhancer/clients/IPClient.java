package com.exercise.enhancer.clients;

import com.exercise.enhancer.domain.AdEntity;

import java.util.concurrent.CompletableFuture;

public interface IPClient {
  boolean isBlocked(String ip);

  String getIPCountry(String ip);

  CompletableFuture<String> augmentIPCountry(AdEntity adEntity);
}
