package com.exercise.enhancer.clients;

import com.exercise.enhancer.domain.AdEntity;
import com.exercise.enhancer.domain.Demographic;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Component
@Slf4j
public class DemographicClientImpl extends AbstractHttpClient implements DemographicClient {

  @Autowired Executor asyncExecutor;

  @Override
  public Demographic getDemographic(String siteId) {
    url = applicationProperties.getDemographicService().getUrl();
    HashMap<String, String> pathParams = new HashMap<>();
    pathParams.put("siteId", siteId);

    URI requestURI = UriComponentsBuilder.fromUriString(url).buildAndExpand(pathParams).toUri();
    JsonNode response = execute(requestURI, HttpMethod.GET, null);
    return parseResponse(response);
  }

  @Override
  public CompletableFuture<Demographic> augmentDemoGraphic(AdEntity adEntity) {
    return CompletableFuture.supplyAsync(
        () -> {
          log.info("Getting demographic for siteID : " + adEntity.getSite().getId());
          Demographic demographic = getDemographic(adEntity.getSite().getId());
          adEntity.getSite().setDemographic(demographic);
          return demographic;
        },
        asyncExecutor);
  }

  Demographic parseResponse(JsonNode jsonNode) {
    JsonNode demographics = jsonNode.get("demographics");
    if (demographics != null && !demographics.isNull()) {
      long femalePercent = Math.round(demographics.get("pct_female").asLong());
      long malePercent = 100 - femalePercent;
      return new Demographic(String.valueOf(femalePercent), String.valueOf(malePercent));
    }
    return null;
  }
}
