package com.exercise.enhancer.clients;

import com.exercise.enhancer.domain.AdEntity;
import com.exercise.enhancer.domain.Publisher;
import com.exercise.enhancer.exception.PublisherNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Component
@Slf4j
public class PublisherClientImpl extends AbstractHttpClient implements PublisherClient {
  @Autowired Executor asyncExecutor;

  @Override
  public Publisher getPublisher(String siteId) {
    url = applicationProperties.getPublisherService().getUrl();
    URI requestURI = UriComponentsBuilder.fromUriString(url).build().toUri();
    HashMap<String, Object> body = new HashMap();
    HashMap<String, String> siteIdMap = new HashMap<>();
    siteIdMap.put("siteID", siteId);
    body.put("q", siteIdMap);
    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Type", "application/json");
    String request = null;
    try {
      request = new ObjectMapper().writeValueAsString(body);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    HttpEntity<Object> requestEntity = new HttpEntity<>(request, headers);
    JsonNode response = execute(requestURI, HttpMethod.POST, requestEntity);
    return parseResponse(response);
  }

  @Override
  public CompletableFuture<Publisher> augmentPublisher(AdEntity adEntity)
      throws PublisherNotFoundException {
    return CompletableFuture.supplyAsync(
        () -> {
          log.info("Getting publisher for siteID: " + adEntity.getSite().getId());
          Publisher publisher = getPublisher(adEntity.getSite().getId());
          log.info("Publisher found: " + publisher);
          if (publisher == null) {
            throw new PublisherNotFoundException();
          }
          adEntity.setPublisher(publisher);
          return publisher;
        },
        asyncExecutor);
  }

  private Publisher parseResponse(JsonNode response) {
    JsonNode publisher = response.get("publisher");
    if (publisher != null && !publisher.isNull()) {
      return new Publisher(publisher.get("id").asText(), publisher.get("name").asText());
    }
    return null;
  }
}
