package com.exercise.enhancer.clients;

import com.exercise.enhancer.config.ApplicationProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.POJONode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@EnableConfigurationProperties(ApplicationProperties.class)
@Slf4j
public abstract class AbstractHttpClient {
  @Autowired protected ApplicationProperties applicationProperties;
  protected String url;
  @Autowired private RestTemplate restTemplate;

  JsonNode execute(URI uri, HttpMethod method, HttpEntity<Object> requestEntity) {
    try {
      ResponseEntity<String> response =
          restTemplate.exchange(uri, method, requestEntity, String.class);
      if (response.getStatusCode().is2xxSuccessful()) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response.getBody());
      }
    } catch (Exception e) {
      log.error("Exception calling: " + uri.toString() + " " + e.getMessage());
    }
    return new POJONode(null);
  }
}
