package com.exercise.enhancer.clients;

import com.exercise.enhancer.domain.AdEntity;
import com.exercise.enhancer.domain.Demographic;

import java.util.concurrent.CompletableFuture;

public interface DemographicClient {
  Demographic getDemographic(String siteId);

  CompletableFuture<Demographic> augmentDemoGraphic(AdEntity adEntity);
}
