package com.exercise.enhancer.clients;

import com.exercise.enhancer.domain.AdEntity;
import com.exercise.enhancer.domain.Publisher;
import com.exercise.enhancer.exception.PublisherNotFoundException;

import java.util.concurrent.CompletableFuture;

public interface PublisherClient {
  Publisher getPublisher(String siteId);

  CompletableFuture<Publisher> augmentPublisher(AdEntity adEntity)
      throws PublisherNotFoundException;
}
