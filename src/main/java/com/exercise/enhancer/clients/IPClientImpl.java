package com.exercise.enhancer.clients;

import com.exercise.enhancer.config.ApplicationProperties;
import com.exercise.enhancer.domain.AdEntity;
import com.exercise.enhancer.domain.Geo;
import com.maxmind.geoip2.WebServiceClient;
import com.maxmind.geoip2.model.CountryResponse;
import com.maxmind.geoip2.record.Country;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Component
@Slf4j
public class IPClientImpl implements IPClient {
  @Autowired Executor asyncExecutor;

  @Autowired private ApplicationProperties applicationProperties;

  @Override
  @Cacheable("ipAddressCache")
  public boolean isBlocked(String ip) {
    return !"US".equalsIgnoreCase(getIPCountry(ip));
  }

  @Override
  @Cacheable("ipAddressCache")
  public String getIPCountry(String ip) {

    String countryCode = null;
    try (WebServiceClient client =
        new WebServiceClient.Builder(
                applicationProperties.getIpService().getAccount(),
                applicationProperties.getIpService().getLicenseKey())
            .connectTimeout(1000)
            .build()) {

      InetAddress ipAddress = InetAddress.getByName(ip);
      CountryResponse response = client.country(ipAddress);
      Country country = response.getCountry();
      countryCode = country.getIsoCode();
    } catch (Exception e) {
      log.error("Exception getting Geo information. {}", e.getMessage());
    }
    return countryCode;
  }

  @Override
  public CompletableFuture<String> augmentIPCountry(AdEntity adEntity) {
    return CompletableFuture.supplyAsync(
        () -> {
          log.info("Getting Country for IP: " + adEntity.getDevice().getIp());
          String ip = adEntity.getDevice().getIp();
          String ipCountry = getIPCountry(ip);
          if (ipCountry != null) {
            adEntity.getDevice().setGeo(new Geo(ipCountry));
          }
          return ipCountry;
        },
        asyncExecutor);
  }
}
