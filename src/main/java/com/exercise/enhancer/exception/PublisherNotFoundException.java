package com.exercise.enhancer.exception;

public class PublisherNotFoundException extends RuntimeException {
  public PublisherNotFoundException() {
    super("Publisher not found");
  }
}
