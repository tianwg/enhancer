package com.exercise.enhancer.exception;

public class NotAllowedException extends RuntimeException {
  public NotAllowedException() {
    super("Access Not allowed from outside US");
  }
}
