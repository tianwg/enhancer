package com.exercise.enhancer.exception;

import lombok.Data;

@Data
public class ErrorMessage {
  private String message;

  public ErrorMessage() {
    super();
  }

  public ErrorMessage(String message) {
    this.message = message;
  }
}
