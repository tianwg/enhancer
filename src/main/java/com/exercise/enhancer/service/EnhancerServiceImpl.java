package com.exercise.enhancer.service;

import com.exercise.enhancer.clients.DemographicClient;
import com.exercise.enhancer.clients.IPClient;
import com.exercise.enhancer.clients.PublisherClient;
import com.exercise.enhancer.domain.AdEntity;
import com.exercise.enhancer.domain.Demographic;
import com.exercise.enhancer.domain.Publisher;
import com.exercise.enhancer.exception.PublisherNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class EnhancerServiceImpl implements EnhancerService {
  @Autowired PublisherClient publisherClient;
  @Autowired DemographicClient demographicClient;

  @Autowired IPClient ipClient;

  @Override
  public AdEntity enhanceEntity(AdEntity adEntity) throws Exception {
    CompletableFuture<Publisher> publisherCompletableFuture =
        publisherClient.augmentPublisher(adEntity);
    CompletableFuture<Demographic> demographicCompletableFuture =
        demographicClient.augmentDemoGraphic(adEntity);
    CompletableFuture<String> stringCompletableFuture = ipClient.augmentIPCountry(adEntity);

    CompletableFuture<Void> combineFutures =
        CompletableFuture.allOf(
            publisherCompletableFuture, demographicCompletableFuture, stringCompletableFuture);
    try {
      combineFutures.join();
    } catch (Exception e) {
      if (e.getCause() instanceof PublisherNotFoundException) {
        throw new PublisherNotFoundException();
      }
    }
    return adEntity;
  }
}
