package com.exercise.enhancer.service;

import com.exercise.enhancer.domain.AdEntity;
import com.exercise.enhancer.exception.PublisherNotFoundException;

public interface EnhancerService {
  AdEntity enhanceEntity(AdEntity adEntity) throws Exception, PublisherNotFoundException;
}
