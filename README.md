## Run Instruction
***Run application***: 

```./gradlew bootRun```

***Run test*** 

```./gradlew clean test -info```

```EnhancerApplicationTests.testAugmentedResult()``` is used to test the request augmentation. Input is from the requirement docs. And expected result is from cherry picking manually executions.

```EnhancerApplicationTests.testConcurrentRequests()``` used for testing concurrent requests. Not well implemented. But the execution time can be views from the console.

***Note*** When application starts, first request takes longer time which is more than 500ms, but subsequent will be around 200 ms.
Sometimes, the test will fail and ```4XX``` error is returned, it's because the publisher or demographic service sometimes return ```500``` error. For Demographic error, it's properly handled so that the response still returns, but for the publisher service, if ```500``` is received, then it's treated as not found and the transaction is aborted and return ```404```.

## Primary Objectives
- [x] Inject the Site Demographics.

- [x] Inject the Publisher details.

- [x] Inject the Country of Origin.

- [x] Demonstrate the latency added < 500ms.

     After application run, execute ```./concurrentTest``` to test latency and concurrent request test. Right now 50 concurrent performance is well implemented. Depend on machine and JVM config,  mostly under 500 ms. Machine and JVM config also affects concurrent performance.

- [x] Provide automated tests to demonstrate the correctness of your implementation.
   
    ```./gradlew clean test -info```
    
## Bonus Objectives
- [x] How fast can you make the end to end execution? Demonstrate the latency of your application.
    
    By executing ```./concurrentTest``` you can see the a sequence of execution times.
    
- [x] Ensure that the application can handle an average of 50 requests per second over an extended period of time.
   
    By executing ```./concurrentTest``` you can see the execution time. Again, the performance also depends on the machine and JVM config.
    
- [x] If request originates from an IP address outside of the United States, then abort the transaction before calling any internal web services and respond with an error message.
   
    Exception with error message ```Access Not allowed from outside US``` and error code ```403``` will be thrown. Implemented in ```AccessInterceptor.java```. Manually tested by deployed to heroku, and changing proxy to IP outside of US.

- [x] If the Publisher ID cannot be obtained, then abort the transaction.
   
    Exception with error message ```Publisher not found``` and error code ```404``` will be thrown and transaction will be aborted. Not able to create test case for this scenario since the service always return value. But it's been manually tested by changing endpoint.

- [ ] Allow individual processing units to easily be installed / uninstalled at run time.

- [x] Provide ways to monitor the health and performance of the application at run time.

    Monitor API is enabled with spring actuator. Endpoint is : ```http://localhost:8080/actuator/```.
    
- [x] Ensure that the application is fault-tolerant. That is, the end-to-end processing of an incoming request should be able to proceed despite the failure of a non-required service.
   
    Every service call is caught when error happens and didn't get throw so that the error won't affect rest of the transaction. Demographic API sometimes return ```500``` errors, so the test is done by manually repetitive ```curl``` requests to see the response without demographic info.
